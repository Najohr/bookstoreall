package id.co.indivara.jdt12.bookstoreall.controller;

import id.co.indivara.jdt12.bookstoreall.exception.BookNotFoundException;
import id.co.indivara.jdt12.bookstoreall.model.WebResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ErrorHandlerController extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(NoHandlerFoundException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return super.handleNoHandlerFoundException(ex, headers, status, request);
    }

    @ExceptionHandler(BookNotFoundException.class)
    public ResponseEntity<WebResponse<String>> notFoundHandler(BookNotFoundException b, ServletWebRequest request){
        return ResponseEntity.status(HttpStatus.OK).body(
                WebResponse.<String>builder()
                        .message(b.getMessage())
                        .data(null)
//                        .data(request.getRequest().getRequestURI()) // kalo mau ada path
                        .build()
        );
    }


}
