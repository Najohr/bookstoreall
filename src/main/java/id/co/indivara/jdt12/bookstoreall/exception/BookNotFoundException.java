package id.co.indivara.jdt12.bookstoreall.exception;

public class BookNotFoundException extends RuntimeException {

    public BookNotFoundException(Long id){
        super("maaf buku dengan kode: " + id + " tidak ada");
    }

}
