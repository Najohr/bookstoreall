package id.co.indivara.jdt12.bookstoreall.exception;

import java.util.Set;

public class BookUnsupportedFieldPatchException extends RuntimeException{
    public  BookUnsupportedFieldPatchException(Set<String> keys){
        super("maaf field " + keys.toString() + " tidak boleh di update");
    }
}
