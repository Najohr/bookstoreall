package id.co.indivara.jdt12.bookstoreall.repository;

import id.co.indivara.jdt12.bookstoreall.entity.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface BookRepository extends JpaRepository<Book, Long> {}
