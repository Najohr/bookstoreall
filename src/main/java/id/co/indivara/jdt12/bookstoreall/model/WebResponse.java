package id.co.indivara.jdt12.bookstoreall.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder

public class WebResponse <T>{
    private String message;
    private T data;
}
