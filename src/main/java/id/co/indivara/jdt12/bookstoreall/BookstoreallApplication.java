package id.co.indivara.jdt12.bookstoreall;

import id.co.indivara.jdt12.bookstoreall.entity.Book;
import id.co.indivara.jdt12.bookstoreall.repository.BookRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

import java.math.BigDecimal;

@SpringBootApplication
public class BookstoreallApplication extends SpringBootServletInitializer{

	/*
	 * Biar bisa running di tomcat, pertama deklarasiin class yang ngeextends SpringBootServletInitializer
	 * kemudian override method Configure, terus masukin classnya ke return builder.sources(NamaMainClass.class)
	 * kemudian ke pom.xml terus cari dependency terus masukkin:
	   <dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-tomcat</artifactId>
			<scope>test</scope>
		</dependency>
	 * Terus masukkin ini di dalem <build></build>
	     <finalName>contoh</finalName>
	 * Terus taro di root
	  	<packaging>war</packaging>
	 */

	public static void main(String[] args) {
		SpringApplication.run(BookstoreallApplication.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) { // biar bisa runnig di tomcat
		return builder.sources(BookstoreallApplication.class);
	}

//	@Profile("pertamakalijalan")
//	@Bean
//
//	CommandLineRunner initDB(BookRepository bookRepository){
//		return args -> {
//			bookRepository.save(Book.builder()
//							.name("najmi")
//							.author("waduh")
//							.price(new BigDecimal("100"))
//					.build());
//			bookRepository.save(Book.builder()
//					.name("najmi2")
//					.author("waduh")
//					.price(new BigDecimal("100"))
//					.build());
//			bookRepository.save(Book.builder()
//					.name("najmi3")
//					.author("waduh")
//					.price(new BigDecimal("100"))
//					.build());
//		};
//	}
}
