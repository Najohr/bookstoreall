package id.co.indivara.jdt12.bookstoreall.controller;

import id.co.indivara.jdt12.bookstoreall.entity.Book;
import id.co.indivara.jdt12.bookstoreall.exception.BookNotFoundException;
import id.co.indivara.jdt12.bookstoreall.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
public class BookController {

    @Autowired
    private BookRepository bookRepository;

    @GetMapping("/waduh")
    public String waduh(){
        return "waduh";
    }

    @GetMapping("/books")
    public List<Book> findAll(){
        return bookRepository.findAll();
    }

    @GetMapping("/books/{id}")
    public Book find(@PathVariable(name = "id") Long id){
        return bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException(id));
    }

    @PutMapping("/books")
    public String create(@RequestBody @Valid Book b){
        bookRepository.save(
                Book.builder()
                        .name(b.getName())
                        .author(b.getAuthor())
                        .price(b.getPrice())
                .build()
        );

        return "success";
    }

}
