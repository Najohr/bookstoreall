package id.co.indivara.jdt12.bookstoreall.entity;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "books")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class Book {
    @Id
    @GeneratedValue
    private Long id;

    @NotEmpty(message = "name wajib di isi")
    private String name;
    @NotEmpty(message = "author wajib di isi")
    private String author;

    @NotNull(message = "price wajib di isi")
    @DecimalMin("1.00")
    private BigDecimal price;
}
